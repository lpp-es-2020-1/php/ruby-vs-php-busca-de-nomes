FROM php:7.4-cli
COPY . /usr/src/ruby-vs-php-2
WORKDIR /usr/src/ruby-vs-php-2
CMD [ "php", "./index.php" ]