## Documentação

Documentação do desafio: [PHP vs Ruby: busca de nomes](https://turing.inf.ufg.br/mod/forum/discuss.php?d=2008)

## Instalação

Execute os seguintes comandos para executar o código via Docker

```bash
docker build -t ruby-vs-php-2 .
docker run -it --rm --name ruby-vs-php-2 ruby-vs-php-2
```