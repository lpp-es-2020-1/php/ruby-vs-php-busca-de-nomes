<?php

// Carregar bibliotecas externas
require_once 'vendor/autoload.php';

$faker = Faker\Factory::create();
//$faker->addProvider(new Faker\Provider\pt_BR\Person($faker)); 

$filename = "database.txt";

return false;
# CUIDADO! CONTINUAR EXCLUIRÁ O ARQUIVO

# Criar e abrir arquivo
$dataset = fopen($filename, "w");

# Quantidade de registros, em milhões
$m = 66;

$start_time = microtime(true);

for ($i = 1; $i <= $m; $i++) {

    $t0 = microtime(true);

    for ($j = 0; $j < 1000000; $j++) {
        fwrite($dataset, $faker->name.PHP_EOL);
    }

    $t1 = microtime(true);
    $elapsed = round($t1 - $t0, 4);
    echo $i."/".$m." registros inseridos em ".$elapsed."s...\n";
}

$end_time = microtime(true);
$elapsed = round($end_time - $start_time, 4);

echo "Dataset criado com sucesso em ".$elapsed."s";
fclose($dataset);