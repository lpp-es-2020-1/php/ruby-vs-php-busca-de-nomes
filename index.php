<?php

// Carregar bibliotecas externas
require_once 'vendor/autoload.php';

$faker = Faker\Factory::create();
$faker->addProvider(new Faker\Provider\pt_BR\Person($faker)); 

$nomes = [];
$qtde_nomes = 1000000; // Quantidade de nomes aleatórios a ser gerado

for ($i = 0; $i < $qtde_nomes; $i++) {
    array_push($nomes, $faker->name);
}

$t0 = microtime(true); // Iniciar cronômetro

// Remover nomes repetidos
$nomes_unicos = array_unique($nomes);

$t1 = microtime(true); // Encerrar cronômetro
$tempo_decorrido = round($t1 - $t0, 4);

echo ($qtde_nomes - count($nomes_unicos)).' nomes repetidos encontrados. ';
echo 'Código executado em '.$tempo_decorrido.'s.';