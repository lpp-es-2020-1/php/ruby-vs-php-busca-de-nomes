<?php

/**
 * Remover pasta e seus arquivos
 * @param [type] $dir
 * @return void
 */
function rrmdir($dir) {

    if (is_dir($dir)) {

        $objects = scandir($dir);

        foreach ($objects as $object) {
            
            if ($object != "." && $object != "..") {
                
                if (filetype($dir . "/" . $object) == "dir") {
                    rrmdir($dir."/".$object);
                
                } else {
                    unlink($dir."/".$object);
                }
            }
        }

        reset($objects);
        rmdir($dir);
    }
}

/**
 * Contar quantas linhas tem o arquivo
 * @param [type] $filename
 * @return void
 */
function count_file_lines($filename) {

    $handle = fopen($filename, "r");
    $count = 0;

    while (!feof($handle)) {
        $line = fgets($handle);
        $count++;
    }

    return $count;
}

function remove_duplicates($filename, $loop = 0, $remove = false) {

    echo "\n";
    $start_time = microtime(true);

    $file = fopen($filename, "r");
    $count = 0;
    $names_repeated = 0;

    $temp_dir = "temp";
    rrmdir($temp_dir); # Remover pasta
    mkdir($temp_dir); # Criar pasta

    while (true) {

        $names = [];
        $chunk_size = 500000;

        for ($i = 1; $i < $chunk_size; $i++) {

            $line = fgets($file);

            if ($line !== false) {
                array_push($names, $line);

            } else {
                break;
            }
        }

        if (count($names) > 0) {

            $names_length = count($names);
            $names = array_unique($names);

            $names_diff = $names_length - count($names);
            $names_repeated += $names_diff;

            $temp_name = "temp/dataset" . $count . ".txt";
            echo "${names_diff} nomes repetidos foram removidos. Salvando arquivo temporário '${temp_name}'...\n";

            # Criar arquivo temporário
            file_put_contents($temp_name, implode("", $names));

        // Foi chegado ao fim do arquivo
        } else {
            break;
        }

        $count++;

        if ($count == 3) {
            //break;
        }
    }

    fclose($file);

    $elapsed = round(microtime(true) - $start_time, 3);
    echo "Leitura do arquivo '${filename}' finalizada em ${elapsed}seg - ${names_repeated} nomes repetidos encontrados.\n";

    if ($remove) {
        unlink($filename);
        echo "Arquivo '${filename}' removido.\n";
    }

    $t1 = microtime(true);

    $files = scandir($temp_dir);
    $file_concat_name = "concat_${loop}.txt";
    $file_concat = fopen($file_concat_name, "w");

    foreach ($files as $f) {

        if ($f != "." && $f != "..") {
            fwrite($file_concat, file_get_contents($temp_dir."/".$f));
        }
    }

    fclose($file_concat);

    $elapsed = round(microtime(true) - $start_time, 3);
    echo "Novo arquivo '${file_concat_name}' gerado em ${elapsed}seg\n";

    # Chamar novamente a função, mas dessa vez para o novo arquivo gerado
    if ($names_repeated > 0) {
        remove_duplicates($file_concat_name, $loop + 1, true);

    } else {

        echo "\nFIM DA EXECUÇÃO!!\n";
        
        $unique_name = "dataset-unique.txt";
        rename($file_concat_name, $unique_name);

        $file_lines = count_file_lines($unique_name);
        echo "Arquivo final gerado '${filename}' - ${file_lines} linhas\n";
    }
}

#$filename = "dataset.txt";
#$file_lines = count_file_lines($filename);
#echo "Iniciando leitura do arquivo '${filename}' - ${file_lines} linhas\n";

remove_duplicates("dataset.txt");
